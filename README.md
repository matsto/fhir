dated on 05/09/2022

# Data Engineer - Solution

## Disclaimer
the solution is not polished due to lack of time.

It was build with scalability in mind.
Uses Redis as a queue and "n" number of workers.
The observer service "mocks" behaviour of lambda triggers.
The solution should be fairly easy to convert it into AWS Lambda or AWS Glue.
The modular architecture should make it easy to extend.

Input files after they are processed are archived in `archive_data` of audit and data retention perposes.

Final files are produced into `output_data`.

## TODO
before it deployed to production:
* improve test coverage
* add integration tests
* change how the secrets are stored, sugested solution AWS SecretManager
* change the final output format from CSV to either parquet or Postres tables, depending what's used down-stream to it


## Installation
 * in the main project folder run `make init`
 * run `make build up`
 * run `make down` to stop the project

## Tests
 * run `make pytest`

## Scale
If the number of workers should be adjasted use `make scale n=N` where N is a new number of workers.
By default there are 4 workers.

