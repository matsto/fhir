from stages.base import Stage
from models import File

def run_stage(stage: Stage, file: File):
    s = stage()
    s.run(file)
