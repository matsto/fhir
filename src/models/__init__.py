from pydantic import BaseModel


class File(BaseModel):
    filename: str

    @property
    def uid(self):
        return self.filename.rstrip(".json")
