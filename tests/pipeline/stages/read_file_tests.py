from stages import read_file as rf
from tempfile import TemporaryDirectory
from models import File
from unittest.mock import Mock
from pathlib import Path

TESTDATA_PATH = "./tests/pipeline/stages/testdata"

import shutil


def test_read_file():
    rf.shutil.move = Mock(side_effect=shutil.copy)
    f = File(filename="Everett935_Littel644_8a3247d3-a54c-43f2-2c5d-a8f5e28ff588.json")
    with TemporaryDirectory() as ar_dir, TemporaryDirectory() as tg_dir:
        read_file = rf.ReadFile(TESTDATA_PATH, tg_dir, ar_dir)
        read_file.run(f)
        t = Path(tg_dir)

        t_f = [f.name for f in t.iterdir()]
        a = Path(ar_dir)
        a_f = [f.name for f in a.iterdir()]
    assert len(t_f) == 1
    assert t_f == ["Everett935_Littel644_8a3247d3-a54c-43f2-2c5d-a8f5e28ff588"]
    assert len(a_f) == 1
    assert a_f == ["Everett935_Littel644_8a3247d3-a54c-43f2-2c5d-a8f5e28ff588.json"]
