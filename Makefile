init:
	python3 -m venv venv
	./venv/bin/pip install -U pip wheel
	./venv/bin/pip install -r requirements_dev.txt

up:
	./venv/bin/docker-compose --env-file env/combined.env up -d

down:
	./venv/bin/docker-compose down --remove-orphans

prune:
	docker system prune -a
	docker volume prune

config:
	./venv/bin/docker-compose --env-file env/combined.env config

build:
	./venv/bin/docker-compose --env-file env/combined.env build --parallel

logs:
	./venv/bin/docker-compose logs -f

restart_rebuild: down build up

restart: down up

scale:
	./venv/bin/docker-compose --env-file env/combined.env up --scale worker=$(n) -d

pytest:
	PYTHONPATH=${PWD}/src ./venv/bin/pytest
