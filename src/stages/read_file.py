from .base import Stage
from fhir.resources.bundle import Bundle
import pandas as pd
from constants import Paths
import os
import shutil
from models import File


class ReadFile:
    name = "ReadFile"

    def __init__(
        self,
        load_folder=Paths.LOAD_PATH,
        TAGET_PATH=Paths.DATA_PATH,
        archive_folder=Paths.ARCHIVE_PATH,
    ):
        self._load_folder = load_folder
        self._TAGET_PATH = TAGET_PATH
        self._archive_folder = archive_folder

    def _read(self, file: File) -> pd.DataFrame:
        self._load_path = os.path.join(self._load_folder, file.filename)

        b = Bundle.parse_file(self._load_path)
        return pd.DataFrame([a.dict() for a in b.entry])

    def _save(self, file: File, df: pd.DataFrame) -> None:
        pickle_path = os.path.join(self._TAGET_PATH, file.uid)
        df.to_pickle(pickle_path)

    def _archive(self, file: File):
        arch_path = os.path.join(self._archive_folder, file.filename)
        shutil.move(self._load_path, arch_path)

    def run(self, file: File) -> None:
        df = self._read(file)
        self._save(file, df)
        self._archive(file)
