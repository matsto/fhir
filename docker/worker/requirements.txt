pandas>=1.4.4
fhir.resources>=6.4.0
pydantic>=1.10.1

rq>=1.11.0
hiredis>=2.0.0
redis>=4.3.4
python-redis-lock>=3.7.0 

fastparquet>=0.8.3 
