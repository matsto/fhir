import os

from redis import Redis
from rq import Queue


def init_redis():
    redis_url = os.getenv("REDIS_URL")
    if not redis_url:
        raise ValueError("Provide Redis URL")
    return Redis.from_url(url=redis_url)


def init_rq() -> Queue:
    return Queue(connection=init_redis())
