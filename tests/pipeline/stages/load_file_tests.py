from stages.load_file import LoadFile
from tempfile import TemporaryDirectory
from models import File
from unittest.mock import Mock
from pathlib import Path
import pandas as pd
import os

TESTDATA_PATH = "./tests/pipeline/stages/testdata"


def test_load_file():
    f = File(filename="Everett935_Littel644_8a3247d3-a54c-43f2-2c5d-a8f5e28ff588")
    m = Mock()
    LoadFile._add_data_to_table = m
    lf = LoadFile(TESTDATA_PATH)
    lf.run(f)
    m.assert_called_once()
    actual_df = m.call_args_list[0].args[0]
    expected_df = pd.read_pickle(
        os.path.join(TESTDATA_PATH, "load_file_expected.pickle")
    )
    assert expected_df.drop(columns=[0]).compare(actual_df).shape == (0, 0)
