from unittest.mock import Mock

from models import File
from observer import observer
from run_stage import run_stage
from stages.load_file import LoadFile
from stages.read_file import ReadFile


def test_observer():
    test_path = "./tests/observer_testdata"
    rq_queue = Mock()
    rq = Mock(return_value=rq_queue)
    observer(test_path, rq)
    assert rq_queue.enqueue.call_count == 2
    actual = [p.args for p in rq_queue.enqueue.call_args_list]

    assert [
        (run_stage, ReadFile, File(filename="correct.json")),
        (run_stage, LoadFile, File(filename="correct.json")),
    ] == actual
