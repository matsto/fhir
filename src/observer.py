from pathlib import Path
from typing import Callable

from rq import Queue

from constants import Paths
from databases.redis_queue import init_rq
from models import File
from run_stage import run_stage
from stages.load_file import LoadFile
from stages.read_file import ReadFile


def observer(path=Paths.LOAD_PATH, rq: Callable[[...], Queue] = init_rq):
    rq_queue = rq()
    _dir = Path(path)
    for f in _dir.iterdir():
        if f.is_dir() or f.suffix != ".json":
            continue
        _file = File(filename=f.name)

        rf = rq_queue.enqueue(run_stage, ReadFile, _file)
        rq_queue.enqueue(run_stage, LoadFile, _file, depends_on=[rf])


if __name__ == "__main__":
    observer()
