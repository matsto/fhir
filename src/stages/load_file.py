import os
import pandas as pd
from .base import Stage
from models import File
from constants import Paths
from databases.redis_queue import init_redis
import redis_lock


class LoadFile(Stage):
    name = "LoadFile"

    def __init__(
        self,
        data_folder=Paths.DATA_PATH,
        traget_folder=Paths.TAGET_PATH,
    ):
        self._data_folder = data_folder
        self._target_folder = traget_folder

    @staticmethod
    def _flatten(df: pd.DataFrame) -> pd.DataFrame:
        for c in [col for col, dt in df.dtypes.items() if dt == object]:
            _df = df[c].apply(pd.Series)
            df = df.drop(columns=c)
            df = df.merge(_df, left_index=True, right_index=True)

        df = df.drop(columns=[0])
        return df

    @staticmethod
    def _add_patient_id(df: pd.DataFrame) -> pd.DataFrame:
        _pat_id = df[df["resourceType"] == "Patient"].id
        if _pat_id.shape != (1,):
            raise ValueError
        pat_id = _pat_id.iat[0]
        df = df.assign(patient_id=pat_id)
        return df

    def _add_data_to_table(self, df: pd.DataFrame) -> None:
        uniq_res_types = df["resourceType"].unique()
        _exist = None
        for t in uniq_res_types:
            _df = df[df["resourceType"] == t]
            filename = os.path.join(self._target_folder, t)
            with redis_lock.Lock(init_redis(), t):
                try:
                    _exist = pd.read_csv(filename)
                except FileNotFoundError:
                    pass
                if _exist is not None and not _exist.empty:
                    _df = pd.concat([_df, _exist], ignore_index=True)
                _df =_df.convert_dtypes()
                _df.to_csv(filename)
                # _df.to_parquet(filename)

    def run(self, file: File) -> None:
        pickle_path = os.path.join(self._data_folder, file.uid)
        df = pd.read_pickle(pickle_path)
        df = self._flatten(df)
        df = self._add_patient_id(df)
        self._add_data_to_table(df)
