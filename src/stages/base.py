from abc import ABC, abstractmethod
from models import File


class Stage(ABC):
    name: str = None

    @abstractmethod
    def run(self, file: File) -> None:
        pass
